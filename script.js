$jui = $jui || {};
$jui.juitabs = {
    onJqReady: function(data) {
        if ( !juiwindata.widgetsExists.juitabs ) {
            return;     // no tabs on that page
        }

        juiwindata.oninit = true;
        delete juiwindata.waiting.juitabs;
        var tabslist = $jq('.juitabs-widget', data['context']);
        tabslist.each(function() {
            try {
                var tabswidget = $jq(this);
                if (tabswidget.data('ready') || $jui.widget.getClosestNotReady(tabswidget)) return;

                var vars = tabswidget.data('juivars') || {}
                ,   juiflags = tabswidget.data('juiflags') || {}
                ,   state  = vars.state || {}
                ,   jopts  = vars.juitabs || {}
                ,   oopts  = widgetOpts(jopts.opts)
                ,   fillspace = juiflags.fillspace || false
                ,   keep_sel = typeof oopts.selected === 'number' ? oopts.selected : -1
                ,   vresizer  = vars.resizer || {}
                ,   rflags    = vresizer.addFlags || {}
                ;
                if (juiflags.nostorestate) _clearState(state);
                if (rflags.nostorestate && state.resizer) delete state.resizer;
                vars.state = state;
                
                oopts.selected = -1;    // no selection frst
                // *********************************************
                // create tabs-object
                tabswidget.tabs(oopts).data('self', tabswidget);
                // *********************************************
                if (keep_sel < 0 && !oopts.collapsible) keep_sel = 0;
                oopts.selected = keep_sel;  // restore
                if (juiflags.nostorestate) state.selected = false;
                state.selected = typeof state.selected === 'number'
                                 ? state.selected : keep_sel;

                var dtabs = tabswidget.data('tabs')
                ,   headerul = dtabs.list
                ,   headers  = dtabs.lis
                ,   options  = dtabs.options
                ,   panels   = dtabs.panels
                ,   wfuncs   = widgetFuncs()
                ,   bfuncs   = bookFuncs(tabswidget)
                ,   closest  = $jui.widget.getClosest(tabswidget)
                ,   closestContent  = closest ? $jui.widget.getClosestContent(tabswidget) : false
                ,   resizable = fillspace ? false : juiflags.resizable || false
                ,   style  = jopts.style || false
                ,   size   = $jui.resizer.getSize(state.resizer, style, fillspace)
                ,   height = size.height.value || false
                ,   width  = size.width.value  || false
                ;

                if (closest) closest = closest.data('self');
                tabswidget.extend({plugin:      vars.plugin,
                                   headerul:    headerul,
                                   headers:     headers,
                                   options:     options,
                                   panels:      panels,
                                   juiwidget:   tabswidget,
                                   juistate:    state,
                                   juivars:     vars,
                                   juiwfuncs:   wfuncs,
                                   juibfuncs:   bfuncs,
                                   juiclosest:  closest,
                                   juiflags:    juiflags,
                                   rflags:      rflags
                });
                    
                // add/remove class, attr, style
                // for widget, header and panels
                var head      = vars.head || {}
                ,   content   = vars.content || {}
                ,   sections  = vars.sections || {}
                ;
                $jui.widget.setAttrCss(tabswidget, jopts);
                $jui.widget.setAttrCss(headers, head);
                $jui.widget.setAttrCss(panels, content);
                $jui.widget.setSectsAttrCss(headers, sections, 'head');
                $jui.widget.setSectsAttrCss(panels, sections, 'content');

                if (juiflags.booktabs) {
                    tabswidget.sellist   = $jq('#' + this.id + '-sellist');
                    bfuncs.initState(state);
                    
                    var toolbar = $jq('#' + this.id + '-tb')
                    ,   navi = $jq('#' + this.id + '-navi')
                    ,   pgtitle = $jq('#' + this.id + '-title')
                    ,   pgof    = $jq('#' + this.id + '-pgof')
                    ,   bsel    = juiflags.nostorestate ? keep_sel + 1: state.booksel || 1
                    ,   pgoftext = bsel + '/' + headers.length
                    ,   title    = $jq(headers[bsel-1]).attr('title')
                    ,   tbwidth = juiflags.nonavi ? 4 : toolbar.width() + 4 || 36
                    ;
                    if (juiflags.notoolbar) { 
                        toolbar.hide();
                    } else if (juiflags.nonavi) {
                        navi.hide();
                        toolbar.css('width', "36");
                    }
                    if (oopts.collapsible) {
                        var clps = $jq('#' + this.id + '-clps');
                        $jui.icon.setPlusMinus(clps, true);
                    }
                    if (pgof.length) pgof.text(pgoftext);
                    if (pgtitle) {
                        var pgtstyle = {};
                        pgtstyle['padding-right'] = tbwidth + 'px !important';
                        $jui.style.mergeElem(pgtitle, pgtstyle, false);
                        pgtitle.text(title);
                    }
                    
                    var vslist = vars.sellist || {}
                    ,   sldock = vslist.opts ? vslist.opts.dock || false : false;
                    if (sldock) {
                        // remove select-buttons
                        $jq('#' + this.id + '-select').remove();
                        // dock and show sellist
                        bfuncs.openSelList(sldock);
                    }
                }
                    
                height = fillspace ? 1 : $jui.resizer.sizeExists(height) ? height : false;
                width  = fillspace ? 1 : $jui.resizer.sizeExists(width)  ? width  : false;

                var hasResizer = fillspace || resizable || height || width;
                if (hasResizer) {
                    tabswidget.extend({hasResizer:true});
                    height = height || 0.8;
                    width  = width  || 1;
                    var ropts = resizable ? vars.resizer || {} 
                                          : {opts: {disabled:true, handles:'none'}}
                        , orszr = resizerOpts(ropts.opts)
                        , funcs = resizerFuncs()
                        , xrszr = $jui.resizer.getExtends(size, funcs)
                    ;
                    tabswidget.resizable(orszr).extend(xrszr).juiAddChildren();
                    tabswidget.extend({juiresizable: tabswidget.data('resizable')});
                }

                // select selected
                tabswidget.tabs("select", state.selected);

                if (hasResizer) {
                    panels.css('overflow', 'auto');
                    if (fillspace && closestContent) {
                        tabswidget.addClass('jui-fillspace');
                        closestContent.addClass('jui-fillspace jui-ohidden');
                    }
                    tabswidget.juiResize();
                    var action = !resizable ? 'disable' : state.selected > -1 ? 'enable' : 'disable';
                    tabswidget.resizable(action);
                }
                
                if (juiflags.booktabs) {
                    $jq('.jui-tb-navi', tabswidget).click(function(e) { 
                        bfuncs.bookNaviClick(this);
                        e.stopImmediatePropagation();
                        return false;
                    });
                    if (oopts.collapsible && clps) {
                        clps.click(function() {
                            bfuncs.bookCollapseClick(this);
                        });                      
                    }
                }

                tabswidget.data('ready', true);
            }catch(e){$jui.dbgAlert('juitabsOnJqReady.each()', e.message);}
            
            function _clearState(state) {
                if (state.selected) delete state.selected;
            }
        });
        juiwindata.oninit = false;


        //
        // functions
        //
        function widgetOpts( opts ) {
            opts = opts || {};
            $jq.extend(opts, {
                select: function (event, ui) {
                    var tabswidget = $jui.widget.getSelf(this)
                    ,   state = tabswidget.juistate
                    ,   booksel = state.selected + 1
                    ,   juiflags = tabswidget.juiflags
                    ;
                    tabswidget.curContent = $jq(ui.panel);
                    if (!juiwindata.oninit) {
                        if (state.booksel) delete state.booksel;
                        state.selected = state.selected != ui.index ? ui.index : -1;
                        if (state.selected == -1) {
                                    // booksel für Filter getrennt 
                            state.booksel = booksel>0 ? booksel : 1;
                            tabswidget.juiResize();
                        }
                        if (!juiflags.nostorestate) {
                            $jui.ajax.storeState($jui.widget.getCleanID(this['id']), state)
                        }
                    }
                    
                    if (juiflags.booktabs) {
                        var pgof = $jq('#' + tabswidget[0].id + '-pgof')
                        ,   pgtitel = $jq('#' + tabswidget[0].id + '-title')
                        ,   cs = tabswidget.juibfuncs.bookPgofCurSel()
                        ,   sel  = state.selected > -1 ? state.selected : cs
                        ,   length = tabswidget.headers.length
                        ,   pgoftext = '' + (sel+1) + '/' + length
                        ,   header   = tabswidget.headers[sel]
                        ,   title    = $jq(header).attr('title')
                        ,   oopts    = tabswidget.options || {}
                        ;
                        if (pgof.length) pgof.text(pgoftext);
                        if (pgtitel.length) pgtitel.text(title);
                        if (oopts.collapsible) {
                            var clps = $jq('#' + this.id + '-clps');
                            $jui.icon.setPlusMinus(clps, (state.selected<0));
                        }
                    }

                    // hooks
                    var cc  = $jui.widget.getClosestContent(tabswidget)
                    ,   cur = tabswidget.curContent;
                    if (cc.length) {
                        cc = cc.data('self');
                        if (cc && cc.juiAjaxPreLoad) cur.juiAjaxPreLoad = cc.juiAjaxPreLoad;
                        if (cc && cc.juiAjaxPostLoad) cur.juiAjaxPostLoad = cc.juiAjaxPostLoad;
                    }
                    // ajax call if needed
                    $jui.ajax.parseContent(this, cur);
                },
                show: function (event, ui) {
                    var tabswidget = $jui.widget.getSelf(this)
                    ,   state = tabswidget.juistate;
                    if (tabswidget.hasResizer) {
                        if (state.selected == -1) {
                            tabswidget.height(tabswidget.headerul.height() + 5);
                            $jui.sbtoggler.resize();
                        } else {
                            tabswidget.juiResize();
                        }
                        if (tabswidget.juiflags.resizable) {
                            tabswidget.resizable(state.selected > -1
                                                    ? 'enable' : 'disable');
                        }
                    }
                }
            });
            return opts;
        }

        function resizerOpts( opts ) {
            opts = opts || {};
            $jq.extend(opts, {
                disabled: true,
                start: function(event, ui) {
                    juiwindata.JuiResizing = true;
                },
                resize: function(event, ui){
                    var tabswidget = $jui.widget.getSelf(this)
                    ,   juisize  = tabswidget.juisize
                    ,   headerul = tabswidget.headerul
                    ,   panels   = tabswidget.panels
                    ,   dy = ui.size.height - juisize.panelheight
                                            - headerul.outerHeight()
                                            - parseFloat(panels.css('padding-top'))
                                            - parseFloat(panels.css('padding-bottom'))
                    ;
                    juisize.panelheight += dy;
                    panels.css('height', juisize.panelheight);
                },
                stop: function(e, ui) {
                    if (!juiwindata.oninit) {
                        var tabswidget = $jui.widget.getSelf(this)
                        ,   juisize = tabswidget.juisize
                        ,   state   = tabswidget.juistate
                        ,   height  = ui.size.height
                        ,   width   = ui.size.width
                        ;

                        state.resizer = juisize.calcSizeVal(height, width, juisize);
                        tabswidget.juiResize();
                        if (!tabswidget.rflags.nostorestate) {
                            $jui.ajax.storeState($jui.widget.getCleanID(this.id), state)
                        }
                    } else {
                        $jui.dbgAlert('tabs.resizerOpts.stop', 'juiwindata.oninit !!!');
                    }
                    juiwindata.JuiResizing = false;
                }
            });
            return opts;
        }

        function widgetFuncs () {
            return {
                tbwrenchClick: function(data) {
                    try {
                        var wgt = data.widget
                        ,   id  = wgt[0].id
                        ,   idx = wgt.juistate.selected
                        ,   header = wgt.headers[idx] || {}
                        ,   size   = wgt.juisize
                        ,   hpx = (size.height.valuePx).toFixed(0)
                        ,   hpc = size.height.valuePc
                        ,   wpx = (size.width.valuePx).toFixed(0)
                        ,   wpc = size.width.valuePc
                        ,   fs  = wgt.juiflags.fillspace || false
                        ,   rszr = wgt.juiflags.resizable || false
                        ,   coll = wgt.options.collapsible
                        ,   titel  = $jq.trim($jq(header).text()) || 'none'
                        ,   pheight = size.panelheight
                        ,   pwgt = wgt.juiclosest
                        ,   parents = pwgt ? '' : 'none'
                        ,   cwgt = wgt.juiChildren
                        ,   children = cwgt ? '' : 'none'
                        ,   cid = wgt.curContent[0].id
                        ,   ajaxsource = wgt.ajax && wgt.ajax[cid] 
                                            ? wgt.ajax[cid].source || 'none' 
                                            : false
                        ,   ajaxpageid = ajaxsource ? wgt.ajax[cid].pageid || false : false
                        ;
                        if (rszr) {
                            var h = wgt.juiresizable.options.handles;
                            rszr += ' (' + h + ')';
                        }

                        while (pwgt) {
                            parents += "\n    -> " + pwgt[0].id;
                            pwgt = pwgt.juiclosest;
                        }
                        for (var cid in cwgt) {
                            children += "\n    -> " + cid
                        }

                        var msg = "ToDo:: Weitere Funktionalität\n"
                                + "(Menü/Tools/Einstellungen/Filter, ...)\n"
                                + "\nwidget = " + id
                                + "\nheight = " + hpx + " / " + hpc
                                + "\nwidth = " + wpx + " / " + wpc
                                + "\nfillspace = " + fs
                                + "\nresizable = " + rszr
                                + "\ncollapsible = " + coll
                                + "\n\nactive = " + titel
                                + "\nid = " + header.id
                                + "\nheight = " + pheight
                                + "\n\nparents: " + parents
                                + "\nchildren: " + children
                                + "\npage: " + wgt.juivars.page
//                                + "\nnamespace: " + NS
                                + "\najax.source: " + ajaxsource
                            ;
                        
                        return {msg:msg, ajaxpageid:ajaxpageid};
                    }catch(e){$jui.dbgAlert('tbwrenchClick()', e.message);}
                }
            }
        }

        function bookFuncs (tabswidget) {
            return {
                tabswidget: tabswidget
            ,   initState: function(state) {
                    if (!state.sellist) state.sellist = {};
                    var sslist = state.sellist;
                    if (!sslist.curlist) sslist.curlist = 'all';
                    if (!sslist[sslist.curlist]) sslist[sslist.curlist] = {};
                
                    var tabswidget  = this.tabswidget
                    ,   vslist      = tabswidget.juivars.sellist || {}
                    ,   addflags    = vslist.addFlags || {}
                    ,   cslist      = sslist[sslist.curlist]
                    ,   nss         = addflags.nostorestate || false
                    ;
                    if (!cslist.height || nss) {
                        cslist.height = vslist.style && vslist.style.height 
                                            ? vslist.style.height : 240;
                    }
                    if (!cslist.width || nss) {
                        cslist.width = vslist.style && vslist.style.width 
                                            ? vslist.style.width : 360;
                    }
                    if (!$jui.$jq.isNumeric(cslist.nofloat) || nss) {
                        cslist.nofloat = addflags.nofloat || 0;
                    }
                }
            ,   bookNaviClick: function(elem, action) {
                    $jui.selClose.fire();
                    if (!elem && !action) return;
                    action = action || $jui.get.actionFromID(elem.id);
                    
                    
                    var tabswidget = this.tabswidget
                    ,   pgof   = $jq('#' + tabswidget[0].id + '-pgof');
                    if (!pgof.length) return;
                    
                    var len = tabswidget.headers.length
                    ,   cs = this.bookPgofCurSel()
                    ,   select  = false
                    ;
                    
                    switch (action) {
                      case 'first': select = cs != 0 ? 0 : false;       break;
                      case 'prev':  select = cs > 0 ? cs-1 : false;     break;
                      case 'next':  select = cs < len-1 ? cs+1 : false; break;
                      case 'last':  select = cs!=len-1 ? len-1 : false; break;
                      case 'select': this.openSelList();    // no break, no new select
                      default: select = false;
                    }
                    
                    if (select || select === 0) {
                                // nicht direkt,
                                // sondern über eigene Listen (All/Filter)
                        tabswidget.tabs("select", select);
                        this.selectItem(select);
                    }
                }
            ,   bookCollapseClick: function(elem) {
                    var tabswidget = this.tabswidget
                    ,   sel = tabswidget.juistate.selected
                    ,   cs = this.bookPgofCurSel()
                    ;
                    if (cs === false) return;
                    
                    sel = sel < 0 ? cs : -1;
                    tabswidget.tabs("select", sel);
                    $jui.icon.setPlusMinus(elem, sel<0 )
                }
            ,   bookPgofCurSel: function() {
                    var tabswidget = this.tabswidget
                    ,   pgof = $jq('#' + tabswidget[0].id + '-pgof')
                    ,   txt  = pgof.text() || '1/1';
                    if (!pgof.length) return false;
                    var cs = parseInt(txt.split('/')[0]) - 1;
                    return cs < 0 ? 0 : cs;
                }
            ,   openSelList: function(dock) {
                    dock = dock || false;
                    var tabswidget = this.tabswidget
                    ,   sellist   = tabswidget.sellist
                    ,   slvars    = tabswidget.juivars.sellist || {}
                    ,   slflags   = slvars.addFlags || {}
                    ,   nss       = slflags.nostorestate || false
                    ,   self      = this
                    ,   selected  = self.bookPgofCurSel()
                    ,   floaticon = $jq('.jui-floattoggle-icon', sellist)
                    ,   jsc       = $jui.selClose
                    ,   state     = tabswidget.juistate
                    ;
                    if (!sellist.length) return;
                    
                    if (floaticon.length && slflags.nofloaticon) floaticon.hide();
                    
                    // populate
                    self.populateSelList();
                    // selected
                    self.selectItem(selected);
                    
                    // event-binding
                    var selitems = $jq('.jui-sellist-li', sellist).addClass('jui-float-left')
                    ,   pointer  = 'jui-sellist-pointer';
                    selitems.mouseover(function() {$jq('span', this).addClass(pointer);});
                    selitems.mouseout(function() {$jq('span', this).removeClass(pointer);});
                    selitems.click(function() {
                        jsc.fire();
                        var selected = state.selected
                        ,   select = parseInt($jq(this).attr('selidx'));
                        if (select !== selected) {
                            tabswidget.tabs("select", select);
                            self.selectItem(select);
                        }
                    });
                    

                    var wid = tabswidget[0].id
                    ,   clist = state.sellist.curlist
                    ,   clstate = state.sellist[clist] || {}
                    ;
                    
                    _bindSelitems(selitems, clstate);
                    floaticon.click(function(e) { 
                        selitems.toggleClass('jui-float-left jui-float-none');
                        floaticon.toggleClass('ui-icon-arrowthick-1-e ui-icon-arrowthick-1-s'); 
                        clstate.nofloat = selitems.hasClass('jui-float-none') ? 1 : 0; 
                        _bindSelitems(selitems, clstate);
                        state.sellist[clist] = clstate;
                        if (!nss) $jui.ajax.storeState($jui.widget.getCleanID(wid), state)
                        e.stopImmediatePropagation();
                        return false;
                    });
                    
                    // nofloat
                    if (clstate.nofloat && !selitems.hasClass('jui-float-none')) {
                        selitems.toggleClass('jui-float-left jui-float-none');
                        floaticon.toggleClass('ui-icon-arrowthick-1-e ui-icon-arrowthick-1-s'); 
                    }

                    // resize
                    sellist.juiResize = function() {
                        var body = $jq('.jui-sellist-body', sellist)
                        ,   th = $jq('.jui-sellist-title', sellist).height();
                        body.height(sellist.height() - th);
                    };

                    
                    if (dock) {
                        _dockSelList(dock);
                    } else {
                        var closeicon = $jq('.jui-close-icon', sellist)
                        ,   pinicon   = $jq('.jui-pin-icon', sellist)
                        ;
                        // prepare closing
                        jsc.add(sellist);
                        jsc.setTimeout();
                        sellist.mouseover(function() {jsc.clearTimeout();});
                        sellist.mouseout(function() {jsc.setTimeout(100);});
                    
                        // click-events: close, stop propagation else
                        closeicon.click(function() { 
                            sellist.hide(); 
                        });
                        sellist.click(function(e) {
                            if (e) e.stopImmediatePropagation();
                            return false;
                        });
                        
                        pinicon.removeClass('jui_sbt_marker ui-icon-pin-s')
                               .addClass('ui-icon-pin-w');
                        pinicon.click(function(e) {
                            pinicon.toggleClass('ui-icon-pin-s ui-icon-pin-w')
                            var pinned = pinicon.hasClass('ui-icon-pin-s');
                            if (pinned) {
                                pinicon.addClass('jui_sbt_marker');
                            } else {
                                pinicon.removeClass('jui_sbt_marker');
                                jsc.add(sellist);
                            }
                            sellist.data('pinned',pinned);
                            e.stopImmediatePropagation();
                            return false;
                        });
                        
                        // draggable, resizable
                        sellist.draggable({
                            handle:'.jui-sellist-title', 
                            cancel:'.jui-icon-right'
                        });
                        sellist.resizable({ 
                            handles: 's,w,e,sw',
                            resize: function(e, ui) {
                                sellist.juiResize();
                            },
                            stop: function(e, ui) {
                                sellist.juiResize();
                                clstate.height  = ui.size.height;
                                clstate.width   = ui.size.width;
                                state.sellist[clist] = clstate;
                                if (!nss) $jui.ajax.storeState($jui.widget.getCleanID(wid), state)
                            }
                        });
                        // position
                        sellist.css({left:'',right:'-4px', top:'16px',bottom:''
                                    ,height: clstate.height, width: clstate.width});

                        // reset pinned
                        sellist.data('pinned',false); 
                        sellist.show();
                        sellist.juiResize();
                    }
                    
                
                    function _dockSelList() {
                        var d    = dock.split('-')
                        ,   cls  = d[0] + '-widget' || false
                        ,   pane = d[1] || false
                        ,   lout = tabswidget.juiclosest || {}
                        ,   item = {}
                        ;
                        
                        if (!cls || !pane) {
                            $jui.dbgAlert('sellist.opts.dock = ' + dock);
                            return;
                        }
                        
                        if (!lout.jquery || !lout.hasClass(cls) || !lout.panes[pane]) {
                            return;
                        }

                        // prepare resizing
                        item[sellist[0].id] = sellist;
                        if (!lout.juiChildren) lout.juiChildren = {};
                        $jq.extend(lout.juiChildren, item);
                        
                        // hide buttons: close, pin
                        $jq('.jui-close-icon, .jui-pin-icon', sellist).hide();
                        
                        // append to pane and show
                        pane = lout.panes[pane];
                        pane.empty();
                        sellist.detach()
                               .appendTo(pane)
                               .css({'height':'100%','width':'100%'})
                               .show();
                        sellist.juiResize();
                        self.selectItem(state.selected);
                    }
            
                    function _bindSelitems(selitems, clstate) {
                        selitems.mouseover(function() {
                            if (clstate.nofloat) $jq(this).addClass(pointer);
                        });
                        selitems.mouseout(function() {
                            $jq(this).removeClass(pointer);
                        });
                    }
                }   // end openSelList()
            ,   populateSelList: function(more) {
                    more = more || {};
                    var sellist = this.tabswidget.sellist;
                    if (sellist.data('ready') && !more.force) return;
                    
                    var html     = ''
                    ,   list     = this.getSelList() 
                    ,   nofloat  = more.nofloat ? ' jui-float-none' : ''
                    ,   addclass = more.addclass ? ' ' + more.addclass : ''
                    ,   tpl_cls  = 'jui-sellist-li' + nofloat + addclass
                    ,   tpl_li   = "\n" + '<li class="{{class}}" selidx="{{idx}}">' 
                                    + '<span class="jui-sellist-lispan">{{title}}</span></li>'
                    ;
                    
                    $jq.each(list, function(idx, item) {
                        var cls = tpl_cls
                        ,   line = tpl_li;
                        line = line.replace('{{idx}}', item.idx);
                        line = line.replace('{{class}}', cls);
                        line = line.replace('{{title}}', item.title);
                        html += line;
                    });
                            
                    $jq('ul', sellist).html(html);
                    sellist.data('ready', true);
                }
            ,   getSelList: function() {
                    var tabswidget = this.tabswidget
                    ,   state   = tabswidget.juistate
                    ,   clist   = state.sellist.curlist
                    ,   cfilter = state.sellist.curfilter
                    ;
                    if (!tabswidget.sellists) {
                        var all = {};
                        tabswidget.headers.each(function (idx, header) {
                            var title = header.title
                            ,   tags = $jq(header).attr('juitags');
                            all[idx] = {'title':title, 'tags':tags, 'idx':idx}
                        });
                        tabswidget.sellists = {all: all};
                    }
                    
                    var sellists = tabswidget.sellists;
                    if (!sellists[clist]) {
                        alert('FilterListe nicht erstellt: ' + clist);
                    }
                    
                    return sellists[clist];
                }
            ,   selectItem: function(index) {
                    var sellist = this.tabswidget.sellist;
                    if (!sellist.length) return;
                    var ccur = 'jui-sellist-current';
                    $jq('.' + ccur, sellist).removeClass(ccur);
                    $jq('.jui-sellist-li:eq(' + index + ')', sellist).addClass(ccur);
                }
            }
        }       // end bookFuncs()

        function resizerFuncs () {
            return {
                readypx: function(data) {
                    var tabs  = data.rszr || {}
                    ,   spx   = data.sizepx
                    ,   state = tabs.juistate || {}
                    ,   selected = state.selected
                    ,   headerul = tabs.headerul
                    ,   height   = (selected == -1)
                                    ? headerul.height() + 5
                                    : spx.height
                    ;
                    return {height: height, width:  spx.width};
                },
                ready: function(data) {
                    // rszr===tabswidget
                    var tabs = data.rszr
                    ,   headerul  = tabs.headerul
                    ,   panels    = tabs.panels
                    ,   juisize   = tabs.juisize
                    ,   height    = parseInt(tabs.height()
                                      - headerul.outerHeight(true)
                                      - parseFloat(panels.css('padding-top'))
                                      - parseFloat(panels.css('padding-bottom')))
                    ;

                    panels.css({height: height});
                    juisize.panelheight = height;
                }
            }
        }
    }
};

