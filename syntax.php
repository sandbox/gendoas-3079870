<?php
/**
 * DokuWiki Plugin juitabs (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Albin Spreizer <albin@gendoas.de>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

if (!defined('DOKU_TAB')) define('DOKU_TAB', "\t");
if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');

require_once DOKU_PLUGIN.'juiwidget/syntax.php';

class syntax_plugin_juitabs extends syntax_plugin_juiwidget {

    function getType() { return 'substition';}
    function getPType() { return 'block';}
    function getSort() { return 303; }


    public function connectTo($mode) {
        $this->Lexer->addSpecialPattern('<juitabs.+?</juitabs>',$mode,'plugin_juitabs');
    }

    public function handle($match, $state, $pos, $handler){

        // if ( $this->getIsNesting() ) >>> don't abort, data are needed (instructions) !!!

        // options for that widget
        $plugin = $this->getPluginName();
        $data = array('match' => $match);
        list($opts, $content) = explode('>', substr($match, 8, -10), 2);
        // options for the widget itself
        $data['opts'][$plugin] = $opts;
        // more options
        $content = $this->extractOptions($content, $data['opts']);


        // sections
        $pattern = '&<tab-section.+?</tab-section>&is';
        preg_match_all($pattern, $content, $sections);
        $data_sects = array();
        $idx = 0;
        foreach($sections[0] as $section){
            list($opts, $content) = explode('>', substr($section, 12, -14), 2);
            // options for that section itself
            $data_sects[++$idx]['opts']['head'] = $opts;
            // more options
            $data_sects[$idx]['content'] = $this->extractOptions($content, $data_sects[$idx]['opts']);
        }
        $data['sections'] = $data_sects;

                    $msg = "match=$match";
//                    juiDbgMsg("..........handle:: $msg", -1);
//                                $match = $data['match'];
//                                msg('render:: ' . $match);

        return $data;

    }

    public function render($mode, $renderer, $data) {

        // for ajax prevent caching
        $this->ajaxSetCache($renderer);

        if ($mode != 'metadata' && $mode != 'xhtml') {
            return false;
        }

        if ($this->isPreview() && mode == 'metadata') {
            return false;
        }
        
        // nesting not allowed,
        if ( $this->getIsNesting() ) {
            if ($mode == 'xhtml') {
                // show message-page
                $renderer->doc .= $this->showMsgHtml(array('msgkey' => 'no_nesting'));
            }
            return true;
        }

        // section needed
        if ( !count($data['sections']) ) {
            if ($mode == 'metadata') {
                juiDbgMsg('Tabs required.');
            }
            return false;
        }

        // parse options, widget and its sections
        $data['opts'] = $this->parseOptionString($data['opts'], 'widget');
        foreach ($data['sections'] as &$section) {
            $section['opts'] = $this->parseOptionString($section['opts'], 'section');
        }
        unset($section);    // unset the pointer !!!

        $plugin = $this->getPluginName();
        $uid = $data['opts'][$plugin]['uid'];
        $juiid = $this->getWidgetUID($uid, $match, $plugin);;
        $data['plugin'] = $plugin;
        $data['juiid'] = $juiid;



        if ($mode == 'metadata') {
            return $this->prepareJuiVars($juiid, $data, $renderer);
        } else /*($mode == 'xhtml')*/ {
            if ($this->isPreview() || $this->isSyspage()) {
                $this->prepareJuiVars($juiid, $data, $renderer);
            }
            return $this->_renderXhtml($mode, $renderer, $data);
        }
    }

    private function _renderXhtml($mode, &$renderer, &$data) {
        global $ID;

        if($mode != 'xhtml') {
            return false;
        }

        $plugin = $data['plugin'];
        $juiid  = $data['juiid'];
        $scheme = $data['opts'][$plugin]['scheme'];
        $flags  = $data['opts'][$plugin]['addFlags'];
        $flags  = isset($flags) ? explode(' ', $flags) : array();

//                    $msg = "mode=$mode, juiid= $juiid";
//                    juiDbgMsg("..........render:: $msg", 0);

        // tabs-start
        $doc .= juiDbgMsgHtml("\n<!-- tabs-start -->\n");
        $p = array();
        $p['id']    = $juiid;
        $p['class'] = $this->getWidgetClasses($scheme);
        $p['page']  = $ID;
        $att = buildAttributes($p);
        $doc .= "<div $att>\n";

        // tabs-list
        $doc .= juiDbgMsgHtml("<!-- tabs-header -->\n");
        $p = array();
        $p['id']    = $this->getExtUID($juiid, 'ul');
        $p['class'] = $plugin . '-header-ul';
        if (in_array('booktabs', $flags)) $p['class'] .= ' jui-booktabs';
        $att = buildAttributes($p);

        // more header-elements 
        $more = $this->_htmlHeaderMore($juiid, $flags, $data);
        $doc .= DOKU_TAB . "<ul $att>$more\n";

        // tab-items
        $c_dflt = $this->getLang('head_caption');
        $c_dflt = $this->headCaption($data['opts'], $c_dflt);
        foreach( $data['sections'] as $idx => &$section ) {
            $caption = $this->headCaption($section['opts'], "$c_dflt-$idx");
            $title   = $this->headTitle($section['opts'], $caption);
                                        $section['caption'] = $caption;

            $p = array();
            $p['id']    = $this->getExtUID($juiid, $idx, '-h');
            $p['class'] = $plugin . '-header-li';
            $p['title'] = $title;
            $att = buildAttributes($p);
            $cid = $this->getExtUID($juiid, $idx, '-c');
            $doc .= DOKU_TAB . DOKU_TAB . "<li $att><a href=\"#$cid\">$caption</a></li>\n";
        }
                                        unset($section);
        $doc .= DOKU_TAB . "</ul>\n";
//        $doc .= DOKU_TAB . "</div></ul>\n";

        $doc .= juiDbgMsgHtml("<!-- tabs-content -->\n");
        // sections
        $c_dflt = $this->getLang('head_caption');
        $c_dflt = $this->headCaption($data['opts'], $c_dflt);
        foreach( $data['sections'] as $idx => $section ) {
            $doc .= juiDbgMsgHtml(DOKU_TAB . "<!-- tabs-section$idx-start -->\n");

            // section-content
            $p = array();
            $p['id']    = $this->getExtUID($juiid, $idx, '-c');;
            $p['class'] = CLS_JUICONTENT . $plugin . '-content';
            $att = buildAttributes($p);
            $doc .= DOKU_TAB . "<div $att>\n";

            // parse the content
//                    juiDbgMsg("..........renderJuiContent.tab=${section['caption']}, juiid=$juiid", 2);
            $html = $this->renderJuiContent($section['content'], $juiid);
            $doc .= DOKU_TAB . DOKU_TAB . $html . "\n";

            // end section
            $doc .= DOKU_TAB . "</div>" . juiDbgMsgHtml("<!-- tabs-section$idx-end -->\n");
        }
        $doc .= '</div>';
        $doc .= juiDbgMsgHtml("<!-- tabs-end -->\n");

        $renderer->doc .= $doc;
        return true;
    }

    private function _htmlHeaderMore($juiid, $flags, &$data) {
        $html = '';
        $has_booktabs = in_array('booktabs', $flags);
        $has_wrench   = !in_array('nowrench', $flags);
            
        if ($has_wrench || $has_booktabs) {
            $width = 2;
            if ($has_booktabs) {
                // do create navi
                $html .= "\n" . DOKU_TAB . DOKU_TAB
                        .'<div id="' . $this->getExtUID($juiid, 'navi') . '" >';
                $width += 104;
                $html .= "\n" . DOKU_TAB . DOKU_TAB . DOKU_TAB
                        .'<div id="' . $this->getExtUID($juiid, 'first') . '" '
                        .'class="jui-icon-left jui-tb-navi ui-icon-seek-first"'
                        .'></div>';
                $html .= "\n" . DOKU_TAB . DOKU_TAB . DOKU_TAB
                        .'<div id="' . $this->getExtUID($juiid, 'prev') . '" '
                        .'class="jui-icon-left jui-tb-navi ui-icon-seek-prev"'
                        .'></div>';
                $html .= "\n" . DOKU_TAB . DOKU_TAB . DOKU_TAB
                        .'<div id="' . $this->getExtUID($juiid, 'pgof') . '" '
                        .'class="jui-tb-pgof"'
                        .'>&nbsp;</div>';
                $html .= "\n" . DOKU_TAB . DOKU_TAB . DOKU_TAB
                        .'<div id="' . $this->getExtUID($juiid, 'next') . '" '
                        .'class="jui-icon-left jui-tb-navi ui-icon-seek-next"'
                        .'></div>';
                $html .= "\n" . DOKU_TAB . DOKU_TAB . DOKU_TAB
                        .'<div id="' . $this->getExtUID($juiid, 'last') . '" '
                        .'class="jui-icon-left jui-tb-navi ui-icon-seek-end"'
                        .'></div>';
                $html .= "\n" . DOKU_TAB . DOKU_TAB . "</div>";

                // $has_listicon = !in_array('nolisticon', $flags);
                if (!in_array('nolisticon', $flags) ) {
                    // is sellist docking
                    $sldock = false;
                    $sldata = $data['opts']['sellist'];
                    $sld = $sldata['opts']['dock']; 
                    list($sld, $foo) = explode('-', $sld, 2);
                    if ( juiGetIsNesting($sld)) $sldock = true;

                    if (!$sldock && !in_array('nolisticon', $flags)) {
                        $width += 16;
                        $html .= "\n" . DOKU_TAB . DOKU_TAB
                                .'<div id="' . $this->getExtUID($juiid, 'select') . '" '
                                .'class="jui-icon-left jui-tb-navi ui-icon-circle-triangle-s"'
                                .'></div>';
                    }

                    // sellist
    // kein dock-filter               $html .= $this->htmlDivSellist($juiid, $sldata, $sldock, DOKU_TAB . DOKU_TAB);
                    $html .= $this->htmlDivSellist($juiid, $sldata, DOKU_TAB . DOKU_TAB);
                }
            }
            
            if ($has_wrench ) {
                $width += 16;
                $html .= "\n" . DOKU_TAB . DOKU_TAB
                        .'<div id="' . $this->getExtUID($juiid, 'tbw') . '" '
                        .'class="jui-icon-left jui-tbwrench ui-icon-none"'
                        .'></div>';
            }
            
            $html = "\n" . DOKU_TAB
                        .'<div id="' . $this->getExtUID($juiid, 'tb') . '" '
                        .'class="jui-toolbar jui-abs-se" '
                        .'style="width:' . $width . 'px">'
                        . $html
                        . "\n" . DOKU_TAB . '</div>';
        }

        if ($has_booktabs) {
            if (in_array('collapsible', $flags)) {
                $html .= "\n" . DOKU_TAB . DOKU_TAB
                        .'<div id="' . $this->getExtUID($juiid, 'clps') . '" '
                        .'class="juitabs-collapse jui-abs-n2w jui-icon-left" '
                        .'></div>';
            }
            $html .= "\n" . DOKU_TAB . DOKU_TAB
                    .'<div id="' . $this->getExtUID($juiid, 'title') . '" '
                    .'class="juitabs-title"'
                    .'>&nbsp;</div>';
        }
        
        return $html;
    }

    /*
     * Testfunktionen
     */
    private function testGetContent($file) {
        return file_get_contents(DOKU_PLUGIN_JUITABS . '___test/' . $file);
    }

}

// vim:ts=4:sw=4:et:
