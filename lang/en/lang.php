<?php
/**
 * german language file
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author      Albin Spreizer <albin@gendoas.de>
 */
$lang['no_nesting']         = '~~nested tab-contols not supported~~';
$lang['head_caption']   	= 'Section';
$lang['js']['juitabstest']	= 'This is a js-testvariable'; 

$lang['selection'] = 'Selection';
?>
