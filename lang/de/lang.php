<?php
/**
 * german language file
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author      Albin Spreizer <albin@gendoas.de>
 */
$lang['no_nesting']   		= '~~Tab-Controls können nicht geschachtelt werden~~';
$lang['head_caption']   	= 'Tab';
$lang['js']['juitabstest']	= 'Dies ist eine JS-Testvariable'; 

$lang['selection'] = 'Auswahl';
?>
